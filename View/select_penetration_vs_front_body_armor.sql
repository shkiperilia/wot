SELECT *
		, vc.tier
		, vc.cnt
      ,(select count (*) *100/vc.cnt
		 from vehicles 
	    where vehicles.tier = v.tier
	      and armor_body_front < (v.penetration) ) as penetration_armor_body_front
	      ,(select count (*) *100/vc.cnt
		 from vehicles 
	    where vehicles.tier = v.tier
	      and v.armor_body_front < (penetration) ) as armor_body_front_penetr_by
FROM [WoT].[dbo].[vehicles] v
join vehCntByTier vc
  on vc.tier = v.tier

order by 10 desc