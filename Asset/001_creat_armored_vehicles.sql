drop table vehicles;

go;

create table vehicles
	(
	vehicles_id int not null primary key IDENTITY (1,1),
	name nvarchar(100) not null,
	country nvarchar(100) not null,
	type nvarchar (100) not null,
	tier int not null,
	armor_body_front int not null,
	armor_body_sides int not null,
	--armor_body_rear int not null,
	penetration int not null
	)